package com.zjt.geoquiz;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class QuizActivity extends ActionBarActivity {
	private static final String CURRENT_QUESTION_INDEX = "CURRENT_QUESTION_INDEX";

	private final String TAG = this.getClass().getSimpleName();

	private TextView questionTextView;
	private Button trueButton;
	private Button falseButton;
	private ImageButton previousButton;
	private ImageButton nextButton;
	private Button cheatButton;

	private final TrueFalse[] questionBank = new TrueFalse[] {
			new TrueFalse(R.string.question_mars, false),
			new TrueFalse(R.string.question_mountain, true),
			new TrueFalse(R.string.question_river, false),
			new TrueFalse(R.string.question_singapore, true),
			new TrueFalse(R.string.question_solar, false),
			new TrueFalse(R.string.question_helium, false) };
	private int currentQuestionIndex = 0;

	private boolean isCheater = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate(Bundle) called ...");
		setContentView(R.layout.activity_quiz);

		if (savedInstanceState != null) {
			currentQuestionIndex = savedInstanceState
					.getInt(CURRENT_QUESTION_INDEX);
		}

		initialize();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			ActionBar actionBar = getActionBar();
			actionBar.setSubtitle("API Level " + Build.VERSION.SDK_INT);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause() called ...");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume() called ...");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop() called ...");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy() called ...");
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(CURRENT_QUESTION_INDEX, currentQuestionIndex);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (intent == null) {
			return;
		}
		isCheater = intent.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN,
				false);
	}

	private void initialize() {
		initializeQuestionTextView();
		initializeTrueButton();
		initializeFalseButton();
		initializePreviousButton();
		initializeNextButton();
		initializeCheatButton();
	}

	private void initializePreviousButton() {
		previousButton = (ImageButton) findViewById(R.id.previousButton);
		previousButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentQuestionIndex = (questionBank.length
						+ currentQuestionIndex - 1)
						% questionBank.length;
				questionTextView.setText(questionBank[currentQuestionIndex]
						.getQuestions());
			}
		});
	}

	private void initializeNextButton() {
		nextButton = (ImageButton) findViewById(R.id.nextButton);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentQuestionIndex = (currentQuestionIndex + 1)
						% questionBank.length;
				questionTextView.setText(questionBank[currentQuestionIndex]
						.getQuestions());

				isCheater = false;
			}
		});
	}

	private void initializeCheatButton() {
		cheatButton = (Button) findViewById(R.id.cheatButton);
		cheatButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean answerIsTrue = questionBank[currentQuestionIndex]
						.isTrue();
				Intent intent = new Intent(QuizActivity.this,
						CheatActivity.class);
				intent.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE,
						answerIsTrue);
				startActivityForResult(intent, 0);
			}
		});
	}

	private void initializeQuestionTextView() {
		questionTextView = (TextView) findViewById(R.id.questionText);
		questionTextView.setText(questionBank[currentQuestionIndex]
				.getQuestions());

		questionTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				currentQuestionIndex = (currentQuestionIndex + 1)
						% questionBank.length;
				questionTextView.setText(questionBank[currentQuestionIndex]
						.getQuestions());
			}
		});
	}

	private void initializeTrueButton() {
		trueButton = (Button) findViewById(R.id.trueButton);
		trueButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				checkAnswer(true);
			}
		});
	}

	private void initializeFalseButton() {
		falseButton = (Button) findViewById(R.id.falseButton);
		falseButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				checkAnswer(false);
			}
		});
	}

	private void checkAnswer(boolean userClickTrueButton) {
		if (isCheater) {
			display(R.string.judgment_toast);
		} else {
			if (userClickTrueButton == questionBank[currentQuestionIndex]
					.isTrue()) {
				display(R.string.correct_toast);
			} else {
				display(R.string.incorrect_toast);
			}
		}
	}

	private void display(int messageResId) {
		Toast.makeText(QuizActivity.this, messageResId, Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz, menu);
		return true;
	}
}
