package com.zjt.geoquiz;

public class TrueFalse {

	private int questions;
	private boolean isTrue;

	public TrueFalse(int questions, boolean isTrue) {
		this.questions = questions;
		this.isTrue = isTrue;
	}

	public int getQuestions() {
		return questions;
	}

	public void setQuestions(int questions) {
		this.questions = questions;
	}

	public boolean isTrue() {
		return isTrue;
	}

	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}

}
