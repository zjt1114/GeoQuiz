package com.zjt.geoquiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends ActionBarActivity {
	public static final String EXTRA_ANSWER_IS_TRUE = CheatActivity.class
			.getPackage() + ".answer_is_true";

	public static final String EXTRA_ANSWER_SHOWN = CheatActivity.class
			.getPackage() + ".answer_shown";

	private final String TAG = this.getClass().getSimpleName();

	private boolean answerIsTrue;

	private TextView answerTextView;
	private Button showAnswerButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate(Bundle) called ...");
		setContentView(R.layout.activity_cheat);

		initialize();
	}

	private void initialize() {
		setAnswerShownResult(false);
		initializeExtras();
		initializeAnswerTextView();
		initializeShowAnswerButton();
	}

	private void initializeExtras() {
		answerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
	}

	private void initializeAnswerTextView() {
		answerTextView = (TextView) findViewById(R.id.answerTextView);
	}

	private void setAnswerShownResult(boolean isAnswerShown) {
		Intent data = new Intent();
		data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
		setResult(RESULT_OK, data);
	}

	private void initializeShowAnswerButton() {
		showAnswerButton = (Button) findViewById(R.id.showAnswerButton);
		showAnswerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAnswerShownResult(true);

				if (answerIsTrue) {
					answerTextView.setText(R.string.true_button);
				} else {
					answerTextView.setText(R.string.false_button);
				}
			}
		});
	}
}
